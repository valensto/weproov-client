import React, {useCallback} from 'react';
import { Link } from "react-router-dom";

export const ListUser = () => {
	const [users, setUsers] = React.useState([]);
	const [loading, setLoading] = React.useState(false);
	const [error, setError] = React.useState(null);

	const fetchUsers = useCallback(async () => {
		setLoading(true);
		try {
			const response = await fetch('http://localhost:4000/v1/users');
			const data = await response.json();
			setUsers(data);
		} catch (e) {
			setError(e);
		}
		setLoading(false);
	}, [])

	const deleteUser = useCallback(async (id) => {
		try {
			await fetch(`http://localhost:4000/v1/users/${id}`, {
				method: 'DELETE',
			});
			fetchUsers();
		} catch (e) {
			alert(e.message);
		}
	}, [fetchUsers])

	React.useEffect(() => {
		fetchUsers()
	}, [fetchUsers]);

	if (loading) {
		return <div>Loading...</div>;
	}

	if (error) {
		return <div>Error: {error.message}</div>;
	}

	return (
		<div>
			<Link to="/add-user">Add User</Link>
			<h1>Users</h1>
			<ul>
				{users.map((user) => (
					<li key={user.id}>
						{user.firstname} {user.lastname}
						<button style={{marginLeft: 10, marginTop: 5}} onClick={() => deleteUser(user.id)}>x</button>
					</li>
				))}
			</ul>
		</div>
	);
};

