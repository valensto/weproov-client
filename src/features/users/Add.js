import {Formik, Form, Field, ErrorMessage} from 'formik';
import {useCallback} from "react";
import {useNavigate} from "react-router-dom";

export const AddUserForm = () => {
	const navigate = useNavigate()
	const initialValues = {firstname: '', lastname: '', email: '', isServiceAccount: false}

	const onSubmit = useCallback(async (values, {setSubmitting}) => {
		try {
			await fetch('http://localhost:4000/v1/users', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(values)
			});
			navigate('/');
		} catch (e) {
			alert(e.message);
		}
		setSubmitting(false);
	}, [navigate])

	const handleValidation = useCallback((values) => {
		const errors = {};
		if (!values.email) {
			errors.email = 'Required';
		} else if (
			!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
		) {
			errors.email = 'Invalid email address';
		}

		if (!values.firstname) {
			errors.firstname = 'Required';
		}
		if (!values.lastname) {
			errors.lastname = 'Required';
		}
		if (!values.email) {
			errors.email = 'Required';
		}
		return errors;
	}, [])

	return (
		(
			<div>
				<h1>Ajoutez votre utilisateur</h1>
				<Formik
					initialValues={initialValues}
					validate={handleValidation}
					onSubmit={onSubmit}
				>
					{({isSubmitting}) => (
						<Form>
							<Field type="text" name="firstname" placeholder={"prénom"}/>
							<ErrorMessage name="firstname" component="div"/>
							<Field type="text" name="lastname" placeholder={"nom"}/>
							<ErrorMessage name="lastname" component="div"/>
							<Field type="email" name="email" placeholder={"email"}/>
							<ErrorMessage name="email" component="div"/>
							<Field type="checkbox" name="isServiceAccount"/>
							<label htmlFor="isServiceAccount">Compte service</label>
							<ErrorMessage name="isServiceAccount" id={"isServiceAccount"} component="div"/>
							<button type="submit" disabled={isSubmitting}>
								Ajouter
							</button>
						</Form>
					)}
				</Formik>
			</div>
		)
	)
};
