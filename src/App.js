import {Route, Routes} from "react-router-dom";
import Users from "./pages/users";
import NotFound from "./pages/not-found";
import AddUser from "./pages/addUser";

function App() {
  return (
    <Routes>
        <Route index element={<Users />} />
        <Route path="/add-user" element={<AddUser />} />
        <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default App;
